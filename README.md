# Patent image OCR
Implement OCR on patent image.

I use two steps to implement OCR, text detection and text recognition.
1. Split text from image by image processing, and output bounding box of words.
2. Use Tesseract to recognize words.

## Requirements
* Install Tesseract - I use `v5.0.0-alpha.20201127` version, [download page](https://tesseract-ocr.github.io/tessdoc/Home.html)

### Python package
* opencv
* numpy
* pytesseract

## Usage
`$ python main.py`

## Retrain Tesseract
Coming soon...

## License
For open source projects, say how it is licensed.

