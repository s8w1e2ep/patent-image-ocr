"""Demo OCR

    Typical usage example:

    demo(img_list, [], root_path)
    
"""

import csv
import os
from time import time

import cv2
import numpy as np

from Evaluator import Evaluator
from generic import *
from TextOCR import TextOCR


IMG_LIST = ['US10740396-20200811-D00012.png',
            'US10909441-20210202-D00002.png']
MAP_PATH = '../US20190122111A1.txt'
SAVE_PATH = 'results'
SAVE_NAME = 'res_{}'
DETECTION_MODEL_PATH = '../frozen_east_text_detection.pb'
LABEL_PATH = '../ground_truth.csv'


def demo(img_list, ground_truth, root_path):
    """ Run OCR test on image list.

    If ground_truth is not empty, it will evaluate accuracy;
    Otherwise, it only recognizes images and display result.

    Args:
        img_list (list): record image path and image name
        ground_truth (list): record correct OCR answer and whether image is rotated
        root_path (str): root path of images
    """
    if len(ground_truth) != 0:
        # If ground_trunth is not empty, it will calculate accuracy
        # Now, it will use three rotate angles to correct image
        # Finally, it will choose the longest words list as result
        eva = Evaluator()
        timer_start = time()
        for idx, img_path in enumerate(img_list):
            img_name = img_path.split('/')[-1]
            print(f'Image count: {idx + 1}, name: {img_name}')
            full_path = os.path.join(root_path, img_path)
            origin_img = cv2.imread(full_path)

            # rotate 0
            words1 = ocr_processing(origin_img)
            # rotate 90
            rotate_img1 = img_correction(origin_img, 90)
            words2 = ocr_processing(rotate_img1)
            # rotate -90
            rotate_img2 = img_correction(origin_img, -90)
            words3 = ocr_processing(rotate_img2)

            words = []
            is_roate = False

            if len(words1) > len(words2):
                words = words1
            else:
                is_roate = True
                words = words2

            if len(words) < len(words3):
                is_roate = True
                words = words3

            ans_str, rotate = ground_truth[idx]
            eva.evaluate_rotate(rotate, is_roate)
            eva.evaluate(ans_str.split('|'), words, img_path)

        # Evaluation
        print(f'OCR Accuracy: {eva.get_total_acc():.2f}')
        print(f'OCR Accuracy (digit): {eva.get_digit_acc():.2f}')
        print(f'OCR Accuracy (eng): {eva.get_eng_acc():.2f}')
        eva.display_rotate_acc()
        print()
        eva.display_missing_words()
    else:
        timer_start = time()
        for idx, img_path in enumerate(img_list):
            img_name = img_path.split('/')[-1]
            print(f'Image count: {idx + 1}, name: {img_name}')
            full_path = os.path.join(root_path, img_path)
            origin_img = cv2.imread(full_path)

            # rotate 0
            words1 = ocr_processing(origin_img)
            # rotate 90
            rotate_img1 = img_correction(origin_img, 90)
            words2 = ocr_processing(rotate_img1)
            # rotate -90
            rotate_img2 = img_correction(origin_img, -90)
            words3 = ocr_processing(rotate_img2)

            words = []
            is_roate = False
            use_angle = 0

            if len(words1) > len(words2):
                words = words1
            else:
                is_roate = True
                use_angle = 90
                words = words2

            if len(words) < len(words3):
                is_roate = True
                words = words3
                use_angle = -90

            print(f'Rotate angle: {use_angle}')
            print(words)

    print(f'Total execution time: {time() - timer_start} s')


def ocr_processing(origin_img):
    """ Do OCR on image.

    First, preprocessing image and find text location.
    Second, recognize text and get results

    Args:
        origin_img (numpy array): image you want to recognize

    Returns:
        list: OCR result array
    """
    ocr = TextOCR()
    edge_img = preprocessing2(origin_img)
    bboxes, _ = find_boxes(edge_img)
    word_bboxes = group_char_to_word(bboxes)
    # Text recognition
    words = ocr.text_recognition_by_word(origin_img, word_bboxes)
    # Remove redundant words
    wordset = set(words)
    # Sort word list
    words = list(wordset)
    words = sorted(words)

    return words


if __name__ == '__main__':
    ground_truth = []
    img_list = []

    # Load Data 1: Open csv file of ground truth
    # with open(LABEL_PATH, 'r') as csv_file:
    #     rows = csv.reader(csv_file, delimiter=',')
    #     for img_path, ans, rotate in rows:
    #         if (img_path == 'fileName'):
    #             continue
    #         img_list.append(img_path)
    #         ground_truth.append((ans, rotate))

    # demo(img_list, ground_truth, '../20211020 test_image')

    # Load Data 2: Read image from directory
    root_path = '../rotate_test/US20210356289A1/images/'
    for file_name in os.listdir(root_path):
        img_list.append(file_name)
    demo(img_list, [], root_path)
