import cv2
import os
from time import time

from generic import preprocessing2, find_boxes, img_correction, group_char_to_word, save_training_img


def generate_training_data():
    data_path = 'data'
    count = 0
    start = time()

    for dir_name in os.listdir(data_path):
        dir_path = os.path.join(data_path, dir_name)
        if os.path.isdir(dir_path):
            img_path = os.path.join(dir_path, 'images')
            for idx, img_name in enumerate(os.listdir(img_path)):
                print(f'Image count: {idx + 1}, name: {img_name}')
                full_path = os.path.join(img_path, img_name)

                origin_img = cv2.imread(full_path)
                edge_img = preprocessing2(origin_img)
                bboxes, vertical_ratio = find_boxes(edge_img)
                is_roate = (vertical_ratio > 0.5)

                if (is_roate):
                    print(f'Need to rotation. {vertical_ratio:.2f}')
                    origin_img = img_correction(origin_img)
                    edge_img = preprocessing2(origin_img)
                    bboxes, _ = find_boxes(edge_img)

                word_bboxes = group_char_to_word(bboxes)
                save_training_img(origin_img, word_bboxes,
                                  count, './train')
                count += len(word_bboxes)

    print(f'Total execution time: {time() - start} s')


if __name__ == '__main__':
    generate_training_data()
